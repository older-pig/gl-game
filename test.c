#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <math.h>
#include <string.h>
#define PI 3.1415926353
#define P2 PI/2
#define P3 3*PI/2
#define DR 0.0174533

float px,py,pdx,pdy,pa,p2x,p2y,p2dx,p2dy;
int st = 0;
int re;
int sv = 0;

void drawPlayer(float x, float y, float dx, float dy) {
	glColor3f(1,1,0);
	glPointSize(8);
	glBegin(GL_POINTS);
	glVertex2i(x,y);
	glEnd();
}

int mapX = 9; int mapY = 9; int mapS=64;

int map[81]; 

void mapgen(int map[15], int re) {
	int map0[] = 
	{
		1,1,1,1,1,1,1,1,1,
		1,0,1,0,0,1,0,0,1,
		1,0,1,0,0,1,0,0,1,
		1,0,0,0,0,1,0,0,1,
		0,0,0,0,0,1,0,0,0,
		1,0,0,1,1,1,0,0,1,
		1,0,0,1,0,0,0,0,1,
		1,0,0,0,0,0,0,0,1,
		1,1,1,1,1,1,1,1,1,
	};
 	
	int map1[] =
	{
	      	1,1,1,1,1,1,1,1,1,
		1,0,0,0,0,1,0,0,1,
		1,0,0,0,0,1,0,0,1,
		1,0,0,1,0,0,0,0,1,
		0,0,0,1,0,0,0,0,0,
		1,0,1,1,1,1,0,0,1,
		1,0,0,1,0,0,0,0,1,
		1,0,0,1,0,0,1,0,1,
		1,1,1,1,1,1,1,1,1,
	};

	int map2[] =
	{
	      	1,1,1,1,1,1,1,1,1,
		1,0,1,0,0,1,0,0,1,
		1,0,1,0,0,1,0,0,1,
		1,0,1,0,0,0,0,0,1,
		0,0,1,0,0,0,0,0,0,
		1,0,1,1,1,1,1,0,1,
		1,0,0,0,0,0,1,0,1,
		1,0,0,0,0,0,0,0,1,
		1,1,1,1,1,1,1,1,1,
	};

	int map3[] =
	{
		1,1,1,1,1,1,1,1,1,
		1,0,1,0,0,0,1,0,1,
		1,0,1,0,0,0,1,0,1,
		1,0,1,0,1,0,1,0,1,
		0,0,0,0,1,0,0,0,0,
		1,0,0,1,1,1,0,0,1,
		1,0,0,0,1,0,0,0,1,
		1,0,0,0,0,0,0,0,1,
		1,1,1,1,1,1,1,1,1,
	};
	
	int map4[] =
	{  
		1,1,1,1,1,1,1,1,1,
		1,0,0,0,0,1,0,0,1,
		1,0,0,0,0,0,0,0,1,
		1,0,1,0,0,0,1,0,1,
		0,0,1,0,0,0,1,0,0,
		1,0,1,0,1,0,1,0,1,
		1,0,1,0,0,0,1,0,1,
		1,0,0,0,0,0,0,0,1,
		1,1,1,1,1,1,1,1,1,
	};
	
	int map5[] =
	{
	      	1,1,1,1,1,1,1,1,1,
		1,0,1,0,0,0,0,0,1,
		1,0,1,1,1,1,1,0,1,
		1,0,0,0,0,0,0,0,1,
		0,0,0,0,1,0,0,0,0,
		1,0,1,1,1,1,1,0,1,
		1,0,0,0,1,0,0,0,1,
		1,0,0,0,0,0,0,0,1,
		1,1,1,1,1,1,1,1,1,
	};
	
	int map6[] =
	{
	      	1,1,1,1,1,1,1,1,1,
		1,0,0,0,0,0,0,0,1,
		1,0,0,0,0,0,0,0,1,
		1,0,0,0,0,0,0,0,1,
		0,0,0,0,1,0,0,0,0,
		1,0,0,0,0,0,0,0,1,
		1,0,0,0,0,0,0,0,1,
		1,0,0,0,0,0,0,0,1,
		1,1,1,1,1,1,1,1,1,
	};

	int map7[] =
	{
	      	1,1,1,1,1,1,1,1,1,
		1,0,1,0,0,0,0,0,1,
		1,0,1,0,1,1,1,0,1,
		1,0,1,0,1,0,1,0,1,
		0,0,1,0,1,0,1,0,1,
		1,0,1,0,0,0,1,0,1,
		1,0,1,1,1,1,1,0,1,
		1,0,0,0,0,0,0,0,1,
		1,1,1,1,1,1,1,1,1,
	};

	int map8[] =
	{
	      	1,1,1,1,1,1,1,1,1,
		1,0,0,0,0,0,0,0,1,
		1,0,0,0,0,0,0,0,1,
		1,0,0,0,0,0,0,0,1,
		0,0,0,1,0,0,0,0,0,
		1,0,1,1,1,1,1,0,1,
		1,0,0,1,0,0,0,0,1,
		1,0,0,0,0,0,0,0,1,
		1,1,1,1,1,1,1,1,1,
	};
	
	int map9[] =
	{
	      	1,1,1,1,1,1,1,1,1,
		1,0,0,0,0,1,0,0,1,
		1,0,0,0,0,1,0,0,1,
		1,1,1,0,1,1,0,0,1,
		0,0,0,0,0,1,0,0,0,
		1,0,0,0,0,0,0,0,1,
		1,0,0,0,0,1,0,0,1,
		1,0,0,0,0,1,0,0,1,
		1,1,1,1,1,1,1,1,1,
	};

	if (re == 0) {
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				map[i*9+j] = map0[i*9+j];
			}
		}
	}

	if (re == 1) {
		for (int i = 0; i < 15; i++) {
		     	for (int j = 0; j < 15; j++) {
			    	map[i*9+j] = map1[i*9+j];
		     	}
	      	}
       	}

	if (re == 2) {
	      	for (int i = 0; i < 15; i++) {
		     	for (int j = 0; j < 15; j++) {
			    	map[i*9+j] = map2[i*9+j];
		     	}
	      	}
       	}

	if (re == 3) {
		for (int i = 0; i < 15; i++) {
		     	for (int j = 0; j < 15; j++) {
			    	map[i*9+j] = map3[i*9+j];
		     	}
	      	}
       	}

	if (re == 4) {
	      	for (int i = 0; i < 15; i++) {
		     	for (int j = 0; j < 15; j++) {
			    	map[i*9+j] = map4[i*9+j];
		     	}
	      	}
       	}

	if (re == 5) {
	      	for (int i = 0; i < 15; i++) {
		     	for (int j = 0; j < 15; j++) {
			    	map[i*9+j] = map5[i*9+j];
		     	}
	      	}
       	}

	if (re == 6) {
	      	for (int i = 0; i < 15; i++) {
		     	for (int j = 0; j < 15; j++) {
			    	map[i*9+j] = map6[i*9+j];
		     	}
	      	}
       	}

	if (re == 7) {
		sv = 1;
	      	for (int i = 0; i < 15; i++) {
		     	for (int j = 0; j < 15; j++) {
			    	map[i*9+j] = map7[i*9+j];
		     	}
	      	}
       	}

	if (re == 8) {
	      	for (int i = 0; i < 15; i++) {
		     	for (int j = 0; j < 15; j++) {
			    	map[i*9+j] = map8[i*9+j];
		     	}
	      	}
       	}

	if (re == 9) {
	      	for (int i = 0; i < 15; i++) {
		     	for (int j = 0; j < 15; j++) {
				map[i*9+j] = map9[i*9+j];
		     	}
	      	}
       	}
}

void drawMap2D() {
	int x,y,xo,yo;
	for (y = 0; y < mapY; y++) {
		for (x = 0; x < mapX; x++) {
			if (map[y*mapX+x] == 1) {
				glColor3f(1,1,1);
			} else {
				glColor3f(0,0,0);
			}
			xo = x*mapS;
			yo = y*mapS;
			glBegin(GL_QUADS);
			glVertex2i(xo+1,yo+1);
			glVertex2i(xo+1,yo+mapS-1);
			glVertex2i(xo+mapS-1,yo+mapS-1);
			glVertex2i(xo+mapS-1,yo+1);	
		}
	}
}

float dist(float ax, float ay, float bx, float by, float ang) {
	return ( sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay)) );
}

void drawRays3D() {
	int r,mx,my,mp,dof;
	float rx,ry,ra,xo,yo, disT;
	ra = pa - DR*32;
	if (ra < 0)
		ra += 2*PI;
	if (ra > 2*PI) 
		ra -= 2*PI;
	for (r = 0; r < 64; r++) {
		dof = 0;
		float disH = 100000;
		int hx = px;
		int hy = py;
		float aTan = -1/tan(ra);
		if (ra > PI) { //up
			ry = (((int)py >> 6)<<6) - 0.0001;
			rx = (py - ry)*aTan+px;
			yo = -64;
			xo = -yo*aTan;	
		}
		if (ra < PI) { //down
			ry = (((int)py >> 6)<<6) + 64;
			rx = (py - ry)*aTan+px;
			yo = 64;
			xo = -yo*aTan; 
		}
		if (ra == 0 || ra == PI) {
			rx = px;
			ry = py;
			dof = 8;
		}
		while (dof < 8) {
			mx = (int) (rx) >> 6;
			my = (int) (ry) >> 6;
			mp = my * mapX + mx;
			if (mp > 0 && mp < mapX*mapY && map[mp] == 1) { dof = 8; hx = rx; hy = ry; disH = dist(px,py,hx,hy,ra); }
			else { rx += xo; ry += yo; dof += 1; }
		}

		//VERTICAL CHECK LOL
		dof = 0;
		float disV = 10000000;
		int vx = px;
		int vy = py;
  		float nTan = -tan(ra);
		if (ra > P2  && ra < P3) { 
   			rx = (((int)px >> 6)<<6) - 0.0001;
   			ry = (px - rx)*nTan+py;
  	 		xo = -64;
   			yo = -xo*nTan; 
  		}
  		if (ra < P2 || ra > P3) {
   			rx = (((int)px >> 6)<<6) + 64;
	   		ry = (px - rx)*nTan+py;
   			xo = 64;
   			yo = -xo*nTan; 
  		}
  		if (ra == 0 || ra == PI) {
   			rx = px;
	   		ry = py;
   			dof = 8;
  		}
		while (dof < 8) {
   			mx = (int) (rx) >> 6;
   			my = (int) (ry) >> 6;
   			mp = my * mapX + mx;
   			if (mp > 0 && mp < mapX*mapY && map[mp] == 1) { dof = 8; vx = rx; vy = ry; disV = dist(px,py,vx,vy,ra); }
   			else { rx += xo; ry += yo; dof += 1; }
  		}
		
		if (disV < disH) { rx = vx; ry = vy; disT = disV; glColor3f(0,1,0); }
		if (disH < disV) { rx = hx; ry = hy; disT = disH; glColor3f(0,0.7,0); }

  		glLineWidth(1);
	  	glBegin(GL_LINES);
  		glVertex2i(px,py);
  		glVertex2i(rx,ry);
		glEnd();

		//DRAWING 3D
		float ca = pa - ra;
		if (ca < 0) ca += 2*PI;
		if (ca > 2*PI) ca -= 2*PI;
		disT *= cos(ca);
		float lineH = (mapS*320)/disT;
		if (lineH > 640) lineH = 640;
		float lineO = 320-lineH/2;

		glLineWidth(9);
		glBegin(GL_LINES);
		glVertex2i(r*9 + 580, lineO);
		glVertex2i(r*9 + 580, lineH + lineO);
		glEnd();

		ra += DR;
		if (ra < 0)
			ra += 2*PI;
		if (ra > 2*PI) 
			ra -= 2*PI;
	}
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if (st == 1) {
		drawMap2D();
		drawRays3D();
		drawPlayer(px,py,pdx,pdy);
	}
	glutSwapBuffers();
}

void mapper() {
	re = random() % 10;
	sv = 0;
	mapgen(map,re);
	px = 10;
	py = 288;
	pdx = cos(pa)*5;
	pdy = sin(pa)*5;
}

void buttons(unsigned char key, int x, int y) {
	if (key == 'a') { 
		pa-=0.2; 
		if (pa < 0) { 
			pa += 2*PI;
		} 
		pdx = cos(pa)*5;
		pdy = sin(pa)*5;	
	}

	if (key == 'd') {
		pa+=0.2; 
		if (pa > 2*PI) {
			pa -= 2*PI;
		} 
		pdx = cos(pa)*5; 
		pdy = sin(pa)*5; 
	}

	int mx1 = (int)(py+pdy) >> 6;
	int my1 = (int)(px+pdx) >> 6;
	int mp1 = mx1*mapY + my1;
	
	int mx2 = (int)(py-pdy) >> 6;
	int my2 = (int)(px-pdx) >> 6;
	int mp2 = mx2*mapY + my2;


	if (key == 'w' && mp1 < mapX*mapY && map[mp1] != 1) { px += pdx; py += pdy; }

	if (key == 's' && mp2 < mapX*mapY && map[mp2] != 1) { px -= pdx; py -= pdy; }

	for (int i = 0; i < 15; i++) {
        	for (int j = 0; j < 15; j++) {
			if (sv == 1) { 
				if (mp1 == 32) 
					mapper(); 
				sv == 0;
			}
			else if (sv == 0) { 
				if (mp1 == 45) 
					mapper(); 
			}
        	}
	}

	if (key == 'q') { st = 0; }

	glutPostRedisplay();
}

void menu(int val) {
	st = 1;
	if ( val == 0) {	
		mapper();
	} else if (val == 10) {
		exit(0);
	}
	glutPostRedisplay();
}

void output(char *string) {
	glColor3f(0,255,255);
	glRasterPos2f(50,50);
      	int len, i;
      	len = (int)strlen(string);
      	for (i = 0; i < len; i++) {
	    	glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, string[i]);
      	}
}

void init() {
	glClearColor(0.3, 0.3, 0.3, 0);
	gluOrtho2D(0, 1152, 576, 0);
	if (st == 0) {
		char string[] = "Welcome Screen. Right-Click for menu.";
		output(string);
		glutCreateMenu(menu);	
		glutAddMenuEntry("start", 0);
		glutAddMenuEntry("exit", 10);
		glutAttachMenu(GLUT_RIGHT_BUTTON);	
	}
	glutPostRedisplay();
}

int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(1152,576);
	glutCreateWindow("MY GAME");
	init();
	glutDisplayFunc(display);
	glutKeyboardFunc(buttons);
	glutMainLoop();
}
